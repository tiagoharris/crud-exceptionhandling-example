package com.tiago.repository;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tiago.entity.Student;


@RunWith(SpringRunner.class)
@DataJpaTest
public class StudentRepositoryTest {

  @Autowired
  StudentRepository repository;
  
  @Test
  public void whenTwoStudentsMeetTheCriteria_thenReturnStudentList() {
    LocalDate fromDate = LocalDate.parse("2001-01-01");
    LocalDate toDate = LocalDate.parse("2001-03-01");
    
    List<Student> students = repository.findAllStudentsBornBetween(fromDate, toDate);
    
    assertEquals(2, students.size());
  }
}
