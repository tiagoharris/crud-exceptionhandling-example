package com.tiago.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tiago.CrudExceptionhandlingExampleApplication;
import com.tiago.dto.StudentDTO;
import com.tiago.entity.Student;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = CrudExceptionhandlingExampleApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class StudentControllerTest {

  @TestConfiguration
  static class StudentControllerTestContextConfiguration {
    @Bean
    public ObjectMapper objectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper;
    }
  }
  
  @Autowired
  ModelMapper modelMapper;
  
  @Autowired
  ObjectMapper objectMapper;
  
  @Autowired
  private MockMvc mvc;

  @MockBean
  private StudentService service;
  
  private Student student = buildStudent();
  
  private static final String BAD_REQUEST_ERROR_CODE = "Bad Request";
  
  private static final String NOT_FOUND_ERROR_CODE = "Not Found";
  
  private static final String CONFLICT_ERROR_CODE = "Conflict";
  
  private static final String JSON_ERROR_MESSAGE_TEMPLATE = "{\"errorCode\":\"%s\",\"errorMessage\":\"%s\"}";
  
  private static final String JSON_MISSING_PROPERTY_MESSAGE_TEMPLATE = "{\"errorCode\":\"%s\",\"errorMessage\":\"Invalid inputs\",\"errors\":[%s]}";
  
  private static final String MISSING_REQUEST_PARAMETER_TEMPLATE = "Required LocalDate parameter '%s' is not present";
  
  private static final String INVALID_REQUEST_PARAMETER_TEMPLATE = "'%s' should be a valid 'LocalDate' and '%s' isn't";

  @Test
  public void whenGetStudents_thenReturnStudentList() throws Exception {
    Mockito.when(
        service.findAll())
        .thenReturn(buildStudentList());
    
    mvc.perform(get("/api/students")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$", hasSize(1)))
      .andExpect(jsonPath("$[0].id", is(student.getId())))
      .andExpect(jsonPath("$[0].name", is(student.getName())))
      .andExpect(jsonPath("$[0].email", is(student.getEmail())))
      .andExpect(jsonPath("$[0].birthDate", is(student.getBirthDate().toString())));
  }
  
  @Test
  public void whenGetStudentsBornBetweenADateRange_thenReturnStudentList() throws Exception {
    Mockito.when(
        service.findByBirthDateBetween(any(), any()))
        .thenReturn(buildStudentList());
    
    mvc.perform(get("/api/students/bornBetween?fromDate=2000-01-01&toDate=2000-02-01")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$", hasSize(1)))
      .andExpect(jsonPath("$[0].id", is(student.getId())))
      .andExpect(jsonPath("$[0].name", is(student.getName())))
      .andExpect(jsonPath("$[0].email", is(student.getEmail())))
      .andExpect(jsonPath("$[0].birthDate", is(student.getBirthDate().toString())));
  }
  
  @Test
  public void whenGetStudentsBornBetweenADateRange_andFromDateIsNull_thenReturnError() throws Exception {
    mvc.perform(get("/api/students/bornBetween?toDate=2000-02-01")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().string(requestParameterIsMissingJsonErrorMessage("fromDate")))
      .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenGetStudentsBornBetweenADateRange_andToDateIsNull_thenReturnError() throws Exception {
    mvc.perform(get("/api/students/bornBetween?fromDate=2000-02-01")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().string(requestParameterIsMissingJsonErrorMessage("toDate")))
      .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenGetStudentsBornBetweenADateRange_andFromDateIsInvalid_thenReturnError() throws Exception {
    String invalidFromDate = "invalid";
    
    mvc.perform(get("/api/students/bornBetween?fromDate=" + invalidFromDate + "&toDate=2000-02-01")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().string(requestParameterIsInvalidJsonErrorMessage("fromDate", invalidFromDate)))
      .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenGetStudentsBornBetweenADateRange_andToDateIsInvalid_thenReturnError() throws Exception {
    String invalidFromDate = "invalid";
    
    mvc.perform(get("/api/students/bornBetween?fromDate=2000-01-01&toDate=" + invalidFromDate)
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().string(requestParameterIsInvalidJsonErrorMessage("toDate", invalidFromDate)))
      .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostAValidStudent_thenReturnSuccess() throws Exception {
    Mockito.when(
        service.save(any()))
        .thenReturn(student);
    
    mvc.perform(post("/api/student")
        .content(newStudentDTOJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(studentDTOJsonString()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void whenPostExistingStudent_thenReturnError() throws Exception {
    Mockito.when(
        service.save(any()))
        .thenThrow(DataIntegrityViolationException.class);
    
    mvc.perform(post("/api/student")
        .content(newStudentDTOJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(userIsAlreadyRegisteredErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostWithoutBodyRequest_thenReturnError() throws Exception {
    mvc.perform(post("/api/student")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingRequestBodyErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostWithoutNameProperty_thenReturnError() throws Exception {
    mvc.perform(post("/api/student")
        .content(studentDTOWithoutNamePropertyJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingPropertyErrorMessage("name")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostWithoutEmailProperty_thenReturnError() throws Exception {
    mvc.perform(post("/api/student")
        .content(studentDTOWithoutEmailPropertyJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingPropertyErrorMessage("email")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostWithInvalidEmailProperty_thenReturnError() throws Exception {
    mvc.perform(post("/api/student")
        .content(studentDTOWithInvalidEmailPropertyJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(invalidEmailErrorMessage()))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostWithoutBirthDateProperty_thenReturnError() throws Exception {
    mvc.perform(post("/api/student")
        .content(studentDTOWithoutBirthDatePropertyJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(missingPropertyErrorMessage("birthDate")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPostWithInvalidBirthDateProperty_thenReturnError() throws Exception {
    mvc.perform(post("/api/student")
        .content(studentWithInvalidBirthDateJsonString("invalid date"))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(invalidDateErrorMessage("invalid date")))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenPutExistentStudent_thenReturnSuccess() throws Exception {
    Mockito.when(
        service.findById(1))
        .thenReturn(student);
    
    Mockito.when(
        service.save(any()))
        .thenReturn(student);
    
    mvc.perform(put("/api/student/1")
        .content(studentDTOJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());
  }
  
  @Test
  public void whenPutInexistentStudent_thenReturnError() throws Exception {
    Integer inexistentId = 666;
    
    ResourceNotFoundException ex = new ResourceNotFoundException(Student.class.getSimpleName(), "id", inexistentId);
    
    Mockito.when(
        service.findById(any()))
        .thenThrow(ex);
    
    mvc.perform(put("/api/student/" + inexistentId)
        .content(studentDTOJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(studentNotFoundWithIdErrorMessage(inexistentId)))
        .andExpect(status().is4xxClientError());
  }
  
  public void whenPutWithoutIdRequestParameter_thenReturnError() throws Exception {
    mvc.perform(put("/api/student/")
        .content(studentDTOJsonString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());
  }
  
  @Test
  public void whenDeleteInexistentStudent_thenReturnError() throws Exception {
    Integer inexistentId = 666;
    
    ResourceNotFoundException ex = new ResourceNotFoundException(Student.class.getSimpleName(), "id", inexistentId);
    
    doThrow(ex).when(service).delete(inexistentId);
    
    mvc.perform(delete("/api/student/" + inexistentId)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(studentNotFoundWithIdErrorMessage(inexistentId)))
        .andExpect(status().is4xxClientError());
  }
  
  @Test
  public void whenDeleteWithoutIdRequestParameter_thenReturnError() throws Exception {
    mvc.perform(delete("/api/student/")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());
  }
  
  @Test
  public void whenDeleteExistingStudent_thenReturnSuccess() throws Exception {
    Mockito.doNothing().when(service).delete(1);
    
    mvc.perform(delete("/api/student/1")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());
  }
  
  private String requestParameterIsMissingJsonErrorMessage(String requestParameter) {
    return String.format(JSON_ERROR_MESSAGE_TEMPLATE, BAD_REQUEST_ERROR_CODE, String.format(MISSING_REQUEST_PARAMETER_TEMPLATE, requestParameter));
  }

  private String requestParameterIsInvalidJsonErrorMessage(String requestParameter, String invalidValue) {
    return String.format(JSON_ERROR_MESSAGE_TEMPLATE, BAD_REQUEST_ERROR_CODE, String.format(INVALID_REQUEST_PARAMETER_TEMPLATE, requestParameter, invalidValue));
  }
  
  private String missingRequestBodyErrorMessage() {
    return String.format(JSON_ERROR_MESSAGE_TEMPLATE, BAD_REQUEST_ERROR_CODE, "Missing request body");
  }
  
  private String userIsAlreadyRegisteredErrorMessage() {
    return String.format(JSON_ERROR_MESSAGE_TEMPLATE, CONFLICT_ERROR_CODE, "This student is already registered");
  }
  
  private String missingPropertyErrorMessage(String propertyName) {
    return String.format(JSON_MISSING_PROPERTY_MESSAGE_TEMPLATE, BAD_REQUEST_ERROR_CODE, "\"'" + propertyName + "' property is missing\"");
  }
  
  private String invalidEmailErrorMessage() {
    return String.format(JSON_MISSING_PROPERTY_MESSAGE_TEMPLATE, BAD_REQUEST_ERROR_CODE, "\"must be a well-formed email address\"");
  }
  
  private String invalidDateErrorMessage(String invalidDateStr) {
    return String.format(JSON_ERROR_MESSAGE_TEMPLATE, BAD_REQUEST_ERROR_CODE, "Text '" + invalidDateStr + "' could not be parsed at index 0");
  }
  
  private String studentNotFoundWithIdErrorMessage(Integer inexistentId) {
    return String.format(JSON_ERROR_MESSAGE_TEMPLATE, NOT_FOUND_ERROR_CODE, "Student not found with id: '" + inexistentId + "'");
  }
  
  private String newStudentDTOJsonString() throws JsonProcessingException {
    StudentDTO newStudentDTO = buildStudentDTO();
    
    newStudentDTO.setId(null);
    
    return objectMapper.writeValueAsString(newStudentDTO);
  }
  
  private String studentDTOJsonString() throws JsonProcessingException {
    return objectMapper.writeValueAsString(buildStudentDTO());
  }
  
  private String studentDTOWithoutNamePropertyJsonString() throws JsonProcessingException {
    StudentDTO studentDTO = buildStudentDTO();
    
    studentDTO.setName(null);
    
    return objectMapper.writeValueAsString(studentDTO);
  }
  
  private String studentDTOWithoutEmailPropertyJsonString() throws JsonProcessingException {
    StudentDTO studentDTO = buildStudentDTO();
    
    studentDTO.setEmail(null);
    
    return objectMapper.writeValueAsString(studentDTO);
  }
  
  private String studentDTOWithInvalidEmailPropertyJsonString() throws JsonProcessingException {
    StudentDTO studentDTO = buildStudentDTO();
    
    studentDTO.setEmail("invalid email");
    
    return objectMapper.writeValueAsString(studentDTO);
  }
  
  private String studentDTOWithoutBirthDatePropertyJsonString() throws JsonProcessingException {
    StudentDTO studentDTO = buildStudentDTO();
    
    studentDTO.setBirthDate(null);
    
    return objectMapper.writeValueAsString(studentDTO);
  }
  
  private String studentWithInvalidBirthDateJsonString(String invalidDateStr) {
    return "{\"name\":\"tiago\", \"email\":\"tiago@email.com\", \"birthDate\":\"" + invalidDateStr + "\"}";
  }
  
  private Student buildStudent() {
    Student student = new Student();

    student.setId(1);
    student.setName("Tiago");
    student.setEmail("tiago@email.com");
    student.setBirthDate(LocalDate.parse("1984-03-20"));

    return student;
  }
  
  private StudentDTO buildStudentDTO() {
    return modelMapper.map(student, StudentDTO.class);
  }

  private List<Student> buildStudentList() {
    List<Student> students = new ArrayList<Student>();

    students.add(buildStudent());

    return students;
  }
}
