package com.tiago.entity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class StudentTest {

  private Student student = buildStudent();
  
  @Test
  public void whenTwoStudentObjectsAreTheSameObject_thenReturnsTrue() {
    Student studentTwo = student;
    
    assertTrue(student.equals(studentTwo));
    assertTrue(student.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenTwoStudentObjectsHaveTheSameId_thenReturnsTrue() {
    Student studentOne = new Student();
    studentOne.setId(1);
    
    Student studentTwo = new Student();
    studentTwo.setId(1);
    
    assertTrue(studentOne.equals(studentTwo));
    assertTrue(studentOne.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenTwoStudentObjectsHaveSameProperties_thenReturnsTrue() {
    Student studentTwo = buildStudent();
    
    assertTrue(student.equals(studentTwo));
    assertTrue(student.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenTwoStudentObjectsHaveDifferentIds_thenReturnsFalse() {
    Student studentTwo = new Student();
    studentTwo.setId(2);
    studentTwo.setName(student.getName());
    studentTwo.setEmail(student.getEmail());
    studentTwo.setBirthDate(student.getBirthDate());
    
    assertFalse(student.equals(studentTwo));
    assertFalse(student.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenTwoStudentObjectsHaveDifferentNames_thenReturnsFalse() {
    Student studentTwo = new Student();
    studentTwo.setId(student.getId());
    studentTwo.setName("other tiago");
    studentTwo.setEmail(student.getEmail());
    studentTwo.setBirthDate(student.getBirthDate());
    
    assertFalse(student.equals(studentTwo));
    assertFalse(student.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenTwoStudentObjectsHaveDifferentEmails_thenReturnsFalse() {
    Student studentTwo = new Student();
    studentTwo.setId(student.getId());
    studentTwo.setName(student.getName());
    studentTwo.setEmail("other_email@email.com");
    studentTwo.setBirthDate(student.getBirthDate());
    
    assertFalse(student.equals(studentTwo));
    assertFalse(student.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenTwoStudentObjectsHaveDifferentBirthDates_thenReturnsFalse() {
    Student studentTwo = new Student();
    studentTwo.setId(student.getId());
    studentTwo.setName(student.getName());
    studentTwo.setEmail(student.getEmail());
    studentTwo.setBirthDate(LocalDate.parse("2000-02-02"));
    
    assertFalse(student.equals(studentTwo));
    assertFalse(student.hashCode() == studentTwo.hashCode());
  }
  
  @Test
  public void whenComparingToNull_thenReturnsFalse() {
    assertFalse(student.equals(null));
  }
  
  @Test
  public void whenComparedToAnotherRandomObject_thenReturnsFalse() {
    assertFalse(student.equals(new Object()));
  }
  
  private Student buildStudent() {
    LocalDate birthDate = LocalDate.parse("2000-01-01");
    
    Student student = new Student();
    student.setId(1);
    student.setName("tiago");
    student.setEmail("tiago@email.com");
    student.setBirthDate(birthDate);
    
    return student;
  }
}
