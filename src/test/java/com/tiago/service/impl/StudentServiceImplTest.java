package com.tiago.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.tiago.entity.Student;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.repository.StudentRepository;
import com.tiago.service.StudentService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { StudentServiceImpl.class })
public class StudentServiceImplTest {

  @Autowired
  private StudentService service;

  @MockBean
  private StudentRepository repository;
  
  private static final Integer EXISTENT_STUDENT_ID = 1;
  
  private static final LocalDate FROM_DATE = LocalDate.parse("2001-01-01");
  
  private static final LocalDate TO_DATE = LocalDate.parse("2001-03-01");
  
  @Before
  public void setUp() {
    Mockito.when(repository.findById(EXISTENT_STUDENT_ID)).thenReturn(buildStudent());
    
    Mockito.when(repository.findAllStudentsBornBetween(FROM_DATE, TO_DATE)).thenReturn(buildStudentList());
    
    Mockito.when(repository.findAll()).thenReturn(buildStudentList());
    
    Mockito.when(repository.save(any())).thenReturn(buildStudent().get());

  }
  
  @Test(expected = ResourceNotFoundException.class)
  public void whenFindingStudentByNonExistingId_thenRaisesResourceNotFoundException() {
    service.findById(null);
  }
  
  @Test
  public void whenFindingStudentByExistingId_thenReturnStudent() {
    assertNotNull(service.findById(EXISTENT_STUDENT_ID));
  }
  
  @Test
  public void whenFindingStudentsByBirthDateBetween_thenReturnStudentList() {
    assertEquals(2, service.findByBirthDateBetween(FROM_DATE, TO_DATE).size());
  }
  
  @Test
  public void whenFindingAllStudents_thenReturnStudentList() {
    assertEquals(2, service.findAll().size());
  }
  
  @Test
  public void WhenSavingANewStudent_thenReturnNewStudent() {
    Student newStudent = buildStudent().get();

    Student savedStudent = service.save(buildStudent().get());

    assertEquals(newStudent, savedStudent);
  }
  
  @Test
  public void WhenDeletingStudentByExistingId_thenShouldDeleteStudent() {
    Student studentToBeDeleted = buildStudent().get();

    service.delete(EXISTENT_STUDENT_ID);

    verify(repository).delete(studentToBeDeleted);
  }
  
  @Test(expected = ResourceNotFoundException.class)
  public void WhenDeletingStudentByNonExistingId_thenRaisesResourceNotFoundException() {
    service.delete(null);
  }
  
  private Optional<Student> buildStudent() {
    Student student = new Student();
    
    student.setId(EXISTENT_STUDENT_ID);
    student.setName("Tiago");
    student.setEmail("tiago@email.com");
    student.setBirthDate(LocalDate.parse("1984-03-20"));
    
    return Optional.of(student);
  }
  
  private List<Student> buildStudentList() {
    List<Student> students = new ArrayList<Student>();
    
    students.add(buildStudent().get());
    students.add(buildStudent().get());
    
    return students;
  }
}
