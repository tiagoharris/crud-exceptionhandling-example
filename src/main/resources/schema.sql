CREATE TABLE IF NOT EXISTS `student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `birth_date` date not null,
  
  PRIMARY KEY(`id`),
  UNIQUE(`name`, `email`, `birth_date`)
) engine=InnoDB default charset=utf8;