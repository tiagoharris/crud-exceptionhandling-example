package com.tiago.entity;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity for table "Student" 
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Entity(name = "student")
public class Student {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;
  
  private String email;
  
  private LocalDate birthDate;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    
    if (o == null) return false;
    
    if (this.getClass() != o.getClass()) return false;
    
    Student student = (Student) o;

    return Objects.equals(getId(), student.getId())
      && Objects.equals(getName(), student.getName())
      && Objects.equals(getEmail(), student.getEmail())
      && Objects.equals(getBirthDate(), student.getBirthDate());
  }

  @Override
  public int hashCode() {
    int hash = 7;
    
    hash = 31 * hash + Objects.hashCode(id);
    hash = 31 * hash + Objects.hashCode(name);
    hash = 31 * hash + Objects.hashCode(email);
    hash = 31 * hash + Objects.hashCode(birthDate);
    return hash;
  }
}
