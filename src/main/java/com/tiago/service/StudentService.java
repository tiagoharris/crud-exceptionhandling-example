package com.tiago.service;

import java.time.LocalDate;
import java.util.List;

import com.tiago.entity.Student;
import com.tiago.exception.ResourceNotFoundException;

/**
 * Service to manage students.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface StudentService {

  /**
   * Finds a Student by id
   * 
   * @param id
   * @return {@link Student}
   * @throws ResourceNotFoundException if no {@link Student} is found
   */
  Student findById(Integer id);
  
  /**
   * Find all students born between the desired date range
   * 
   * @param fromDate
   * @param toDate
   * @return the list of students
   */
  List<Student> findByBirthDateBetween(LocalDate fromDate, LocalDate toDate);
  
  /**
   * Find all students
   * 
   * @return the list of students
   */
  List<Student> findAll();
  
  /**
   * Saves a student
   * 
   * @param student to be saved
   * @return the saved student
   */
  Student save(Student student);
  
  /**
   * Deletes a student
   * 
   * @param id
   * @throws ResourceNotFoundException if no {@link Student} is found
   */
  void delete(Integer id);
}
