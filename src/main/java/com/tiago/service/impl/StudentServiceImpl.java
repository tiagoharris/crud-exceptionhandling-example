package com.tiago.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiago.entity.Student;
import com.tiago.exception.ResourceNotFoundException;
import com.tiago.repository.StudentRepository;
import com.tiago.service.StudentService;

/**
 * Implements {@link StudentService} interface
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class StudentServiceImpl implements StudentService {
  
  @Autowired
  StudentRepository repository;

  /* (non-Javadoc)
   * @see com.tiago.service.StudentService#findById(java.lang.Long)
   */
  @Override
  public Student findById(Integer id) {
    Student student = repository.findById(id).orElse(null);
    
    if (student == null) {
      throw new ResourceNotFoundException(Student.class.getSimpleName(), "id", id);
    }
    
    return student;
  }

  /* (non-Javadoc)
   * @see com.tiago.service.StudentService#findByBirthDateBetween(java.time.LocalDate, java.time.LocalDate)
   */
  @Override
  public List<Student> findByBirthDateBetween(LocalDate fromDate, LocalDate toDate) {
    return repository.findAllStudentsBornBetween(fromDate, toDate);
  }

  /* (non-Javadoc)
   * @see com.tiago.service.StudentService#findAll()
   */
  @Override
  public List<Student> findAll() {
    return repository.findAll();
  }

  /* (non-Javadoc)
   * @see com.tiago.service.StudentService#save(com.tiago.entity.Student)
   */
  @Override
  public Student save(Student student) {
    return repository.save(student);
  }

  /* (non-Javadoc)
   * @see com.tiago.service.StudentService#delete(java.lang.Long)
   */
  @Override
  public void delete(Integer id) {
    Student student = findById(id);
    
    repository.delete(student);
  }
}
