package com.tiago.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tiago.entity.Student;

/**
 * Repository for {@link Student} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
*/
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> { 
  
  /**
   * Find all students born between a date range
   * 
   * @param fromDate
   * @param toDate
   * @return the list of students
   */
  @Query("SELECT s FROM student s WHERE s.birthDate BETWEEN ?1 and ?2")
  List<Student> findAllStudentsBornBetween(LocalDate fromDate, LocalDate toDate);
}
