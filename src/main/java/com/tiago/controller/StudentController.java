package com.tiago.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tiago.dto.StudentDTO;
import com.tiago.entity.Student;
import com.tiago.service.StudentService;

/**
 * Restful controller responsible for managing students
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RestController
@RequestMapping("/api")
public class StudentController {
  
  @Autowired
  ModelMapper modelMapper;

  @Autowired
  StudentService service;
  
  private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
  
  /**
   * Get all students
   * 
   * @return the list of students
   */
  @GetMapping("/students")
  public List<StudentDTO> getAllStudents() {
    List<Student> students = service.findAll();

    LOGGER.info(String.format("getAllStudents() returned %s records", students.size()));
    
    return students.stream().map(student -> convertToDTO(student)).collect(Collectors.toList());
  }
  
  /**
   * Get all students that were born between the desired date range
   * 
   * @param fromDate
   * @param toDate
   * @return the list of students
   */
  @GetMapping(path = "/students/bornBetween")
  public List<StudentDTO> getAllStudentsThatWereBornBetween(
      @RequestParam(value = "fromDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate,
      @RequestParam(value = "toDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toDate) {
    List<Student> students = service.findByBirthDateBetween(fromDate, toDate);

    LOGGER.info(String.format("getAllStudentsThatWereBornBetween() with fromDate [%s] and toDate [%s] returned %s records", fromDate, toDate, students.size()));
    
    return students.stream().map(student -> convertToDTO(student)).collect(Collectors.toList());
  }

  /**
   * Creates a student
   * 
   * @param studentDTO
   * @return the created student
   */
  @PostMapping("/student")
  public StudentDTO createStudent(@Valid @RequestBody StudentDTO studentDTO) {
    Student student = convertToEntity(studentDTO);

    LOGGER.info("createStudent() called");
    
    return convertToDTO(service.save(student));
  }
  
  /**
   * Updates a student
   * 
   * @param studentId
   * @param studentDTO
   * @return the updated student
   */
  @PutMapping("/student/{id}")
  public StudentDTO updateStudent(@PathVariable(value = "id", required = true) Integer studentId, 
      @Valid @RequestBody StudentDTO studentDTO) {
    studentDTO.setId(studentId);
    Student student = convertToEntity(studentDTO);

    LOGGER.info(String.format("updateStudent() called with id [%s]", studentId));
    
    return convertToDTO(service.save(student));
  }
  
  /**
   * Deletes a student
   * 
   * @param studentId
   * @return 200 OK
   */
  @DeleteMapping("/student/{id}")
  public ResponseEntity<?> deleteStudent(@PathVariable(value = "id") Integer studentId) {
    service.delete(studentId);
    
    LOGGER.info(String.format("deleteStudent() called with id [%s]", studentId));
    
    return ResponseEntity.ok().build();
  }
  
  private StudentDTO convertToDTO(Student student) {
    return modelMapper.map(student, StudentDTO.class);
  }
  
  private Student convertToEntity(StudentDTO studentDTO) {
    Student student = null;
    
    if(studentDTO.getId() != null) {
      student = service.findById(studentDTO.getId());
    }
    
    student = modelMapper.map(studentDTO, Student.class);
    
    return student;
  }
}
